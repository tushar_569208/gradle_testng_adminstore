package com.admin.store;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FunctionRepository {
	
	WebDriver driver;
	String customersuccessmsg;
	
	//adding 1 comment Tushar Singh
		
	public void browserAppLaunch() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F");
		Thread.sleep(4000);
	}

	public void login(String userName, String password) throws InterruptedException
	{
		WebElement uName1 = driver.findElement(By.xpath("//input[@type='email']"));
		uName1.clear();
		Thread.sleep(2000);
		WebElement pwd1 = driver.findElement(By.xpath("//input[@name='Password']"));
		pwd1.clear();
		Thread.sleep(5000);
		
		WebElement uName = driver.findElement(By.xpath("//input[@type='email']"));
		uName.sendKeys(userName);	
		WebElement pwd = driver.findElement(By.xpath("//input[@name='Password']"));
		pwd.sendKeys(password);
		WebElement login = driver.findElement(By.xpath("//input[@class='button-1 login-button']"));
		login.click();
		Thread.sleep(4000);
	}

	public boolean verifyValidLogin()
	{	
			String expTitle = "Dashboard / nopCommerce administration";
			String actTitle = driver.getTitle();
			if(expTitle.equals(actTitle)) {
				return true;
			}else {
				return false;
			}
	}

	public boolean verifyInValidLogin()
	{
		String expTitle = "Dashboard / nopCommerce administration";
		String actTitle = driver.getTitle();
		
		if(expTitle!=(actTitle)) {
			return true;
		}else {
			return false;
		}
	}

	public void addCustomer(String email, String pwd, String fname, String lname, String companyname)
			throws InterruptedException {
		WebElement customer = driver.findElement(By.xpath("//body[@class='skin-blue sidebar-mini']/div[@class='wrapper']/div[@class='main-sidebar']/div[@class='sidebar']/ul[@class='sidebar-menu tree']/li[4]/a[1]/span[1]"));
		customer.click();
		Thread.sleep(4000);
		WebElement customers = driver.findElement(By.xpath("//li[@class='treeview menu-open']//ul[@class='treeview-menu']//li//span[@class='menu-item-title'][contains(text(),'Customers')]"));
		customers.click();
		Thread.sleep(4000);
		WebElement addnewbtn = driver.findElement(By.xpath("//a[@class='btn bg-blue']"));
		addnewbtn.click();
		Thread.sleep(2000);
		WebElement emailtxtbox = driver.findElement(By.xpath("//input[@id='Email']"));
		emailtxtbox.sendKeys(email);
		WebElement pwdtxtbox = driver.findElement(By.xpath("//input[@id='Password']"));
		pwdtxtbox.sendKeys(pwd);
		WebElement firstnametxtbox = driver.findElement(By.xpath("//input[@id='FirstName']"));
		firstnametxtbox.sendKeys(fname);
		WebElement lastnametxtbox = driver.findElement(By.xpath("//input[@id='LastName']"));
		lastnametxtbox.sendKeys(lname);
		WebElement genderradiobtn = driver.findElement(By.xpath("//input[@id='Gender_Male']"));
		genderradiobtn.click();
		WebElement companynametxtbox = driver.findElement(By.xpath("//input[@id='Company']"));
		companynametxtbox.sendKeys(companyname);
		Thread.sleep(3000);
		WebElement savebutton = driver.findElement(By.xpath("//button[@name='save']//i[@class='fa fa-floppy-o']"));
		savebutton.click();
		Thread.sleep(5000);
		WebElement messagesuccess = driver.findElement(By.xpath("//div[@class='alert alert-success alert-dismissable']"));
		customersuccessmsg = messagesuccess.getText();
		Thread.sleep(5000);
		System.out.println(customersuccessmsg);
	}

	public boolean verifyValidCustomerAdd() {
		String expmessage = "The new customer has been added successfully.";
		String actmessage = customersuccessmsg;
		if (expmessage==actmessage) {
			return true;
		} else {
			return false;
		}

	}

	public boolean verifyInValidCustomerAdd() {
		String expmessage = "The new customer has been added successfully.";
		String actmessage = customersuccessmsg;

		if (expmessage != (actmessage)) {
			return true;
		} else {
			return false;
		}
	}

		public void appClose()
		{
			driver.close();
		}

}
