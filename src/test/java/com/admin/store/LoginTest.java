package com.admin.store;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest {
	
	//Testing Jenkins
	WebDriver driver;
	FunctionRepository fr = new FunctionRepository();
	
	@BeforeMethod
	public void appLaunch() throws InterruptedException
	{
		fr.browserAppLaunch();
	}
	
	/*
	 * TC_001: Verifying valid login functionality for Admin store application
	 */
	@Test(priority = 0, enabled=true, description="TC_001: Verifying valid login functionality")
	public void verifyValidLogin() throws InterruptedException
	{
		try {
				fr.login("admin@yourstore.com", "admin");
				Assert.assertEquals(true, fr.verifyValidLogin());
			}
		catch(Exception e) {
				System.out.println(e);
			}
	}
	
	/*
	 * TC_002: Verifying invalid login functionality
	 */
	@Test(priority = 1, enabled=true, description="TC_002: Verifying invalid login functionality")
	public void verifyInvalidLogin() {
		try {
			fr.login("dasd1", "dasd1");
			Assert.assertEquals(true, fr.verifyInValidLogin());
		}
	catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@AfterMethod()
	public void appClose()
	{
		fr.appClose();
	}


}
